import Grid from '../../src/marsRover/grid'

describe('Grid', () => {
    it('0 x 0 is inside grid 5 x 5', () => {
        const grid = new Grid(5, 5)
        expect(grid.isInGrid(0, 0)).toBeTruthy()
    })

    it('5 x 5 is inside grid 5 x 5', () => {
        const grid = new Grid(5, 5)
        expect(grid.isInGrid(5, 5)).toBeTruthy()
    })

    it('6 x 0 is outside grid 5 x 5', () => {
        const grid = new Grid(5, 5)
        expect(grid.isInGrid(6, 0)).toBeFalsy()
    })

    it('0 x 6 is outside grid 5 x 5', () => {
        const grid = new Grid(5, 5)
        expect(grid.isInGrid(0, 6)).toBeFalsy()
    })
})