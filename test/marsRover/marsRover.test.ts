import MarsRover from '../../src/marsRover/marsRover'
import Grid from '../../src/marsRover/grid'

describe('Mars rover', () => {
    it('Move from 0 0 N to 0 1 N', () => {
        const marsRover = new MarsRover('00N','M', new Grid(2, 2))
        expect(marsRover.move()).toBe('01N')
    })

    it('Move from 0 0 N to 0 2 N', () => {
        const marsRover = new MarsRover('00N','MM', new Grid(2, 2))
        expect(marsRover.move()).toBe('02N')
    })

    it('Move from 0 0 N to 1 0 E', () => {
        const marsRover = new MarsRover('00N','RM', new Grid(2, 2))
        expect(marsRover.move()).toBe('10E')
    })

    it('Move from 1 2 N to 1 3 N', () => {
        const marsRover = new MarsRover('12N','LMLMLMLMM', new Grid(3, 3))
        expect(marsRover.move()).toBe('13N')
    })

    it('Move from 2 2 N to 2 3 N when grid is 2 x 2', () => {
        const marsRover = new MarsRover('22N','M', new Grid(2, 2))
        expect(marsRover.move()).toBe('It is out side of grid')
    })
})