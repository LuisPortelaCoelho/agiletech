import RomanNumerals from '../src/romanNumerals'

describe('Test conversion from Arabic numerals to Roman numerals', () => {
        
    it('1 to I', () => {

        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(1)
        
        expect(converted).toBe('I')
    })

    it('2 to II', () => {
        const romanNumerals = new RomanNumerals()
   
        const converted = romanNumerals.convert(2)

        expect(converted).toBe('II')
    })
   
    it('3 to III', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(3)
        
        expect(converted).toBe('III')
    })

    it('4 to IV', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(4)
        
        expect(converted).toBe('IV')
    })

    it('5 to V', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(5)
        
        expect(converted).toBe('V')
    })

    it('6 to VI', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(6)
        
        expect(converted).toBe('VI')
    })

    it('7 to VII', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(7)
        
        expect(converted).toBe('VII')
    })

    it('8 to VIII', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(8)
        
        expect(converted).toBe('VIII')
    })

    it('9 to IX', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(9)
        
        expect(converted).toBe('IX')
    })

    it('10 to X', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(10)
        
        expect(converted).toBe('X')
    })

    it('20 to XX', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(20)
        
        expect(converted).toBe('XX')
    })

    it('30 to XXX', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(30)
        
        expect(converted).toBe('XXX')
    })

    it('40 to XL', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(40)
        
        expect(converted).toBe('XL')
    })

    it('50 to L', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(50)
        
        expect(converted).toBe('L')
    })

    it('60 to LX', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(60)
        
        expect(converted).toBe('LX')
    })

    it('70 to LXX', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(70)
        
        expect(converted).toBe('LXX')
    })

    it('80 to LXXX', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(80)
        
        expect(converted).toBe('LXXX')
    })

    it('90 to XC', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(90)
        
        expect(converted).toBe('XC')
    })

    it('100 to C', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(100)
        
        expect(converted).toBe('C')
    })

    it('200 to C', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(200)
        
        expect(converted).toBe('CC')
    })

    it('300 to CCC', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(300)
        
        expect(converted).toBe('CCC')
    })

    it('400 to CD', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(400)
        
        expect(converted).toBe('CD')
    })

    it('500 to D', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(500)
        
        expect(converted).toBe('D')
    })

    it('600 to DC', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(600)
        
        expect(converted).toBe('DC')
    })

    it('700 to DCC', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(700)
        
        expect(converted).toBe('DCC')
    })

    it('800 to DCCC', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(800)
        
        expect(converted).toBe('DCCC')
    })

    it('900 to CM', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(900)
        
        expect(converted).toBe('CM')
    })

    it('1000 to M', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(1000)
        
        expect(converted).toBe('M')
    })

    it('846 to DCCCXLVI', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(846)
    
        expect(converted).toBe('DCCCXLVI')
    })

    it('1999 to MCMXCIX', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(1999)
    
        expect(converted).toBe('MCMXCIX')
    })

    it('2004 to MMIV', () => {
        const romanNumerals = new RomanNumerals()

        const converted = romanNumerals.convert(2004)
    
        expect(converted).toBe('MMIV')
    })
})