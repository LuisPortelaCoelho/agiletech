import fizzBuzz from '../src/fizzBuzz'

describe('Test fizzbuzz', function() {
    it('value 1 is 1', () => {
        expect(fizzBuzz(1)).toBe('1');
    })

    it('value 2 is 2', () => {
        expect(fizzBuzz(2)).toBe('2');
    })

    it('value 4 is 4', () => {
        expect(fizzBuzz(4)).toBe('4');
    })

    it('value 3 is fizz', () => {
        expect(fizzBuzz(3)).toBe('fizz');
    })

    it('value 5 is buzz', () => {
        expect(fizzBuzz(5)).toBe('buzz');
    })

    it('value 15 is fizzbuzz', () => {
        expect(fizzBuzz(15)).toBe('fizzbuzz');
    })

    it('value 6 is fizz', () => {
        expect(fizzBuzz(6)).toBe('fizz');
    })

    it('value 10 is buzz', () => {
        expect(fizzBuzz(10)).toBe('buzz');
    })

    it('value 30 is fizzbuzz', () => {
        expect(fizzBuzz(30)).toBe('fizzbuzz');
    })
})