import leapYear from '../src/leapYear'

describe('Test leap year', function() {
    it('1901 is not leap year', () => {
        expect(leapYear(1901)).toBe(false)
    })

    it('2020 is leap year', () => {
        expect(leapYear(2020)).toBe(true)
    })

    it('1900 is leap year', () => {
        expect(leapYear(1900)).toBe(false)
    })

    it('2004 is leap year', () => {
        expect(leapYear(2004)).toBe(true)
    })
})