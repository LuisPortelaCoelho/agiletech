import GildedRose from '../../../src/gildedRoseRefactoringKata/gilded-rose';
import Item from '../../../src/gildedRoseRefactoringKata/item'

describe('Gilded Rose', () => {
  it('should foo', () => {
    const gildedRose = new GildedRose([new Item('foo', 0, 0)]);
    const items = gildedRose.updateQuality();

    expect(items[0].name).toBe('foo');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(0)
  });

  it('Add foo1', () => {
    const gildedRose = new GildedRose([new Item('foo1', 0, 0)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo1');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(0)
  });

  it('Add foo2; sellIn=1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('foo2', 1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(0)
  });

  it('Add foo2; sellIn=10; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('foo2', 10, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(0)
  });

  it('Add foo2; sellIn=10; quality= 2', () => {
    const gildedRose = new GildedRose([new Item('foo2', 10, 2)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(1)
  });

  it('Add foo2; sellIn=10; quality= 5', () => {
    const gildedRose = new GildedRose([new Item('foo2', 10, 5)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(4)
  });

  it('Add foo2; sellIn=10; quality= 15', () => {
    const gildedRose = new GildedRose([new Item('foo2', 10, 15)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(14)
  });

  it('Add foo2; sellIn=100; quality= 15', () => {
    const gildedRose = new GildedRose([new Item('foo2', 100, 15)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(99)
    expect(items[0].quality).toBe(14)
  });

  it('Add foo2; sellIn=1000; quality= 15', () => {
    const gildedRose = new GildedRose([new Item('foo2', 1000, 15)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(999)
    expect(items[0].quality).toBe(14)
  });

  it('Add foo2; sellIn=1000; quality= -15', () => {
    const gildedRose = new GildedRose([new Item('foo2', 1000, -15)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(999)
    expect(items[0].quality).toBe(-15)
  });

  it('Add foo2; sellIn=-1000; quality= -15', () => {
    const gildedRose = new GildedRose([new Item('foo2', -1000, -15)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(-1001)
    expect(items[0].quality).toBe(-15)
  });

  it('Add foo2; sellIn=1000; quality= 15', () => {
    const gildedRose = new GildedRose([new Item('foo2', 1000, 15), new Item('foo3', 50, 45)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('foo2');
    expect(items[0].sellIn).toBe(999)
    expect(items[0].quality).toBe(14)
    expect(items[1].name).toBe('foo3');
    expect(items[1].sellIn).toBe(49)
    expect(items[1].quality).toBe(44)
  });

  it('Add Aged Brie; sellIn=1000; quality= 15', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 1000, 15)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(999)
    expect(items[0].quality).toBe(16)
  });

  it('Add Aged Brie; sellIn=50; quality= 15', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 50, 15)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(49)
    expect(items[0].quality).toBe(16)
  });

  it('Add Aged Brie; sellIn=0; quality= 0', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 0)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(2)
  });

  it('Add Aged Brie; sellIn=1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(2)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(1)
    expect(items[0].quality).toBe(1)
  });

  it('Add Aged Brie; sellIn=50; quality= 100', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 50, 100)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(49)
    expect(items[0].quality).toBe(100)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=50; quality= 100', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 50, 100)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(49)
    expect(items[0].quality).toBe(100)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=10; quality= 100', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 10, 100)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(100)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=10; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=5; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(4)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=5; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 5, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(4)
    expect(items[0].quality).toBe(51)
  });

  ///////////////// Backstage
  // -1
  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=-1; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', -1, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(-2)
    expect(items[0].quality).toBe(0)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=-1; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', -1, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(-2)
    expect(items[0].quality).toBe(0)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=-1; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', -1, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(-2)
    expect(items[0].quality).toBe(0)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=-1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', -1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(-2)
    expect(items[0].quality).toBe(0)
  });

  /// 0
  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=0; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 0, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(0)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=0; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 0, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(0)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=0; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 0, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(0)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=0; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 0, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(0)
  });

  /// 1
  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=1; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 1, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=1; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 1, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=1; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 1, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(51)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(4)
  });

  /// 10
  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=10; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=10; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 10, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=10; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 10, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(51)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=10; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 10, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(3)
  });

  /// 11
  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=11; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 11, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=11; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 11, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=11; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 11, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(51)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=11; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 11, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(2)
  });

  /// 12
  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=12; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 12, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=12; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 12, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(50)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=12; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 12, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(51)
  });

  it('Add Backstage passes to a TAFKAL80ETC concert; sellIn=12; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 12, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(2)
  });

  ///////////////// Aged Brie
  // -1
  it('Add Aged Brie; sellIn=-1; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', -1, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-2)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=-1; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', -1, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-2)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=-1; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', -1, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-2)
    expect(items[0].quality).toBe(51)
  });

  it('Add Aged Brie; sellIn=-1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', -1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-2)
    expect(items[0].quality).toBe(3)
  });

  /// 0
  it('Add Aged Brie; sellIn=0; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=0; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=0; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(51)
  });

  it('Aged Brie; sellIn=0; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(3)
  });

  /// 1
  it('Add Aged Brie; sellIn=1; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 1, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=1; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 1, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=1; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 1, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(51)
  });

  it('Add Aged Brie; sellIn=1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(2)
  });

  /// 10
  it('Add Aged Brie; sellIn=10; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 10, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=10; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 10, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=10; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 10, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(51)
  });

  it('Add Aged Brie; sellIn=10; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 10, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(9)
    expect(items[0].quality).toBe(2)
  });

  /// 11
  it('Add Aged Brie; sellIn=11; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 11, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=11; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 11, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=11; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 11, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(51)
  });

  it('Add Aged Brie; sellIn=11; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 11, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(2)
  });

  /// 12
  it('Add Aged Brie; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 12, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=12; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 12, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(50)
  });

  it('Add Aged Brie; sellIn=12; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 12, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(51)
  });

  it('Add Aged Brie; sellIn=12; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 12, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(2)
  });

  ///////////////// Sulfuras, Hand of Ragnaros
  // -1
  it('Add Sulfuras, Hand of Ragnaros; sellIn=-1; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', -1, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(49)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=-1; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', -1, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(50)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=-1; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', -1, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(51)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=-1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', -1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(-1)
    expect(items[0].quality).toBe(1)
  });

  /// 0
  it('Add Sulfuras, Hand of Ragnaros; sellIn=0; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 0, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(49)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=0; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 0, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(50)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=0; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 0, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(51)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=0; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 0, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(0)
    expect(items[0].quality).toBe(1)
  });

  /// 1
  it('Add Sulfuras, Hand of Ragnaros; sellIn=1; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 1, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(1)
    expect(items[0].quality).toBe(49)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=1; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 1, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(1)
    expect(items[0].quality).toBe(50)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=1; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 1, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(1)
    expect(items[0].quality).toBe(51)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=1; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 1, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(1)
    expect(items[0].quality).toBe(1)
  });

  /// 10
  it('Add Sulfuras, Hand of Ragnaros; sellIn=10; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 10, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(49)
  });
//
  it('Add Sulfuras, Hand of Ragnaros; sellIn=10; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 10, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(50)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=10; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 10, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(51)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=10; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 10, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(10)
    expect(items[0].quality).toBe(1)
  });

  /// 11
  it('Add Sulfuras, Hand of Ragnaros; sellIn=11; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 11, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(49)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=11; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 11, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(50)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=11; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 11, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(51)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=11; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 11, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(11)
    expect(items[0].quality).toBe(1)
  });

  /// 12
  it('Add Sulfuras, Hand of Ragnaros; quality= 49', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 12, 49)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(12)
    expect(items[0].quality).toBe(49)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=12; quality= 50', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 12, 50)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(12)
    expect(items[0].quality).toBe(50)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=12; quality= 51', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 12, 51)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(12)
    expect(items[0].quality).toBe(51)
  });

  it('Add Sulfuras, Hand of Ragnaros; sellIn=12; quality= 1', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 12, 1)]);
    const items = gildedRose.updateQuality();
    
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].sellIn).toBe(12)
    expect(items[0].quality).toBe(1)
  });
});