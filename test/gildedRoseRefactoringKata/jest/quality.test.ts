import Quality from "../../../src/gildedRoseRefactoringKata/quality"

describe('Test quality', () => {
    it('Add quality from 1 to 2', () => {
        const quality = new Quality()
        expect(quality.addQuality(1)).toBe(2)
    })

    it('Quality equal to 49 is less than 50', () => {
        const quality = new Quality()
        expect(quality.isQualityLessThan50(49)).toBeTruthy()
    })

    it('Quality equal to 1 is higher than 0', () => {
        const quality = new Quality()
        expect(quality.isQualityHigherThan0(1)).toBeTruthy()
    })

    it('Decrease quality from 2 to 1', () => {
        const quality = new Quality()
        expect(quality.decreaseQuality(2)).toBe(1)
    })
})