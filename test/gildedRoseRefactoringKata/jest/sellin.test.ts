import SellIn from '../../../src/gildedRoseRefactoringKata/sellIn'

describe('Test sell in', () => {
    it('Sell in -1 is less than 0', () => {
        const sellIn = new SellIn()
        expect(sellIn.isLessThan0(-1)).toBeTruthy()
    })

    it('Sell in 1 is not less than 0', () => {
        const sellIn = new SellIn()
        expect(sellIn.isLessThan0(1)).toBeFalsy()
    })

    it('Sell in 1 is less than 6', () => {
        const sellIn = new SellIn()
        expect(sellIn.isLessThan6(1)).toBeTruthy()
    })

    it('Sell in 7 not is less than 6', () => {
        const sellIn = new SellIn()
        expect(sellIn.isLessThan6(7)).toBeFalsy()
    })

    it('Sell in 1 is less than 11', () => {
        const sellIn = new SellIn()
        expect(sellIn.isLessThan11(1)).toBeTruthy()
    })

    it('Decrease sell in', () => {
        const sellIn = new SellIn()
        expect(sellIn.decreaseSellIn(10)).toBe(9)
    })
})

