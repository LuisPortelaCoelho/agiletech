import PrimeFactors from "../src/primeFactor"


describe('Prime factors', () => {
    const primeFactors = new PrimeFactors()
    it('1 is not prime number', () => {
        expect(primeFactors.allPrimeFactors(1)).toBe('It is not prime number')
    })

    it('Prime factor of 2', () => {
        expect(primeFactors.allPrimeFactors(2)).toEqual([2])
    })

    it('Prime factor of 3', () => {
        expect(primeFactors.allPrimeFactors(3)).toEqual([3])
    })

    it('Prime factor of 4', () => {
        expect(primeFactors.allPrimeFactors(4)).toEqual([2, 2])
    })

    it('Prime factor of 5', () => {
        expect(primeFactors.allPrimeFactors(5)).toEqual([5])
    })

    it('Prime factor of 8', () => {
        expect(primeFactors.allPrimeFactors(8)).toEqual([2, 2, 2])
    })

    it('Prime factor of 9', () => {
        expect(primeFactors.allPrimeFactors(9)).toEqual([3, 3])
    })

    it('Prime factor of 11', () => {
        expect(primeFactors.allPrimeFactors(11)).toEqual([11])
    })

    it('Prime factor of 18', () => {
        expect(primeFactors.allPrimeFactors(18)).toEqual([2, 3, 3])
    })

    it('Prime factor of 25', () => {
        expect(primeFactors.allPrimeFactors(25)).toEqual([5, 5])
    })

    it('Prime factor of 64', () => {
        expect(primeFactors.allPrimeFactors(64)).toEqual([2, 2, 2, 2, 2, 2])
    })

    it('Prime factor of 74', () => {
        expect(primeFactors.allPrimeFactors(74)).toEqual([2, 37])
    })
})