import StatsCalculator from '../src/statsCalculator'

describe('Test stats calculator - default array', () => {

    const array = [6, 9, 15, -2, 92, 11]
    const statsCalculator = new StatsCalculator(array)

    it('Minimum value', () => {
        const minimum = statsCalculator.minimumValue()
        expect(minimum).toBe(-2)
    })

    it('Maximum value', () => {
        const maximumValue = statsCalculator.maximumValue()
        expect(maximumValue).toBe(92)
    })

    it('Number of elements in the sequence', () => {
        const numberElementsInSequence = statsCalculator.numberElementsInSequence()
        expect(numberElementsInSequence).toBe(6)
    })

    it('Sum of values', () => {
        const sumOfValues = statsCalculator.sumOfValues()
        expect(sumOfValues).toBe(131)
    })

    it('Average value', () => {
        const averageValue = statsCalculator.averageValue()
        expect(averageValue).toBe(21.833333333333333)
    })
}) 

describe('Test stats calculator - array with zeros', () => {

    const array = [0, 0, 0, 0, 0]
    const statsCalculator = new StatsCalculator(array)

    it('Minimum value', () => {
        const minimum = statsCalculator.minimumValue()
        expect(minimum).toBe(0)
    })

    it('Maximum value', () => {
        const maximum = statsCalculator.maximumValue()
        expect(maximum).toBe(0)
    })

    it('Number of elements in the sequence', () => {
        const numberElementsInSequence = statsCalculator.numberElementsInSequence()
        expect(numberElementsInSequence).toBe(5)
    })

    it('Sum of values', () => {
        const sumOfValues = statsCalculator.sumOfValues()
        expect(sumOfValues).toBe(0)
    })

    it('Average value', () => {
        const averageValue = statsCalculator.averageValue()
        expect(averageValue).toBe(0)
    })
}) 

describe('Test stats calculator - empty array', () => {

    const array = []
    const statsCalculator = new StatsCalculator(array)

    it('Minimum value', () => {
        const minimum = statsCalculator.minimumValue()
        expect(minimum).toBe(Infinity)
    })

    it('Maximum value', () => {
        const maximumValue = statsCalculator.maximumValue()
        expect(maximumValue).toBe(-Infinity)
    })

    it('Number of elements in the sequence', () => {
        const numberElementsInSequence = statsCalculator.numberElementsInSequence()
        expect(numberElementsInSequence).toBe(0)
    })

    it('Sum of values', () => {
        const sumOfValues = statsCalculator.sumOfValues()
        expect(sumOfValues).toBe(0)
    })
  
    it('Average value', () => {
        const averageValue = statsCalculator.averageValue()
        expect(averageValue).toBe(NaN)
    })
}) 