class SellIn {

    private readonly MINIMUM_SELL_IN = 0
    
    public isLessThan0(value: number) {
        return value < this.MINIMUM_SELL_IN
    }

    public isLessThan6(value: number) {
        return value < 6
    }

    public isLessThan11(value: number) {
        return value < 11
    }

    public decreaseSellIn(value: number) {
        return value - 1
    }
}

export default SellIn