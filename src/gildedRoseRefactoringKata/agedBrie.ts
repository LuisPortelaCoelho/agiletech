import Item from './item';
import Product from './product'
import Quality from './quality';
import SellIn from './sellIn'

class AgedBrie implements Product{
    private items: Item

    constructor(items: Item) {
        this.items = items
    }

    public updateQuality() {
        let sellIn = new SellIn()
        let quality = new Quality()

        if (quality.isQualityLessThan50(this.items.quality)) {
            this.items.quality = quality.addQuality(this.items.quality)
        }

        this.items.sellIn = sellIn.decreaseSellIn(this.items.sellIn)

        if (sellIn.isLessThan0(this.items.sellIn) && quality.isQualityLessThan50(this.items.quality)) {
            this.items.quality = quality.addQuality(this.items.quality)
        }

        return this.items;
    }
}

export default AgedBrie