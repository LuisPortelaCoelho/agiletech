import Item from './item';
import Product from './product'

class Sulfuras implements Product{
    private items: Item

    constructor(items: Item) {
        this.items = items
    }

    public updateQuality() {
        return this.items;
    }
}

export default Sulfuras