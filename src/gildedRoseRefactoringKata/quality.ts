class Quality {
    private readonly MINIMUM_QUALITY = 0
    private readonly MAXIMUM_QUALITY = 50
    
    public decreaseQuality(value: number): number {
        return value - 1
    }
    
    public isQualityHigherThan0(value: number): boolean {
        return value > this.MINIMUM_QUALITY
    }
    
    public isQualityLessThan50(value: number): boolean {
        return value < this.MAXIMUM_QUALITY
    }    
    
    public addQuality(value: number): number {
        return value + 1
    }

}

export default Quality