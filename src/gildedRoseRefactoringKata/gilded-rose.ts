import Item from './item'
import Sulfuras from './sulfuras'
import AgedBrie from './agedBrie'
import Backstage from './backstage'
import Standard from './standard'
import Product from './product'


class GildedRose implements Product{
  items: Array<Item>;

  constructor(items = [] as Array<Item>) {
    this.items = items;
  }

  updateQuality() {
    
    for (let index = 0; index < this.items.length; index++) {

      switch (this.items[index].name) {
        case 'Sulfuras, Hand of Ragnaros': {
          let sulfuras = new Sulfuras(this.items[index])
          this.items[index] = sulfuras.updateQuality()
          break
        } 
        case 'Backstage passes to a TAFKAL80ETC concert': {
          var backstage = new Backstage(this.items[index])
          this.items[index] = backstage.updateQuality()
          break
        }
        case 'Aged Brie': {
          var agedBrie = new AgedBrie(this.items[index])
          this.items[index] = agedBrie.updateQuality()
          break
        }
        default: {
          var standard = new Standard(this.items[index])
          this.items[index] = standard.updateQuality()
          break
        }
      }
    }

    return this.items;
  }
}

export default GildedRose