import Product from './product'
import Item from './item'
import SellIn from './sellIn'
import Quality from './quality'

class Backstage implements Product{

    private items: Item
    
    constructor(items: Item) {
        this.items = items
    }

    public updateQuality() {
        let sellIn = new SellIn()
        let quality = new Quality()

        if (quality.isQualityLessThan50(this.items.quality)) {
            this.items.quality = quality.addQuality(this.items.quality)            
        }

        if (quality.isQualityLessThan50(this.items.quality) && sellIn.isLessThan11(this.items.sellIn)) {
            this.items.quality = quality.addQuality(this.items.quality)
        }

        if (quality.isQualityLessThan50(this.items.quality) && sellIn.isLessThan6(this.items.sellIn)) {
            this.items.quality = quality.addQuality(this.items.quality)
        }

        this.items.sellIn = sellIn.decreaseSellIn(this.items.sellIn)

        if (sellIn.isLessThan0(this.items.sellIn)) {
            this.items.quality = 0
        }

        return this.items;
    }
}

export default Backstage
