import Item from './item';
import Product from './product'
import Quality from './quality';
import SellIn from './sellIn';

class Standard implements Product{
    private items: Item

    constructor(items: Item) {
        this.items = items
    }
    
    public updateQuality() {
        let sellIn = new SellIn()
        let quality = new Quality()

        if (quality.isQualityHigherThan0(this.items.quality)) {
            this.items.quality = quality.decreaseQuality(this.items.quality)
        }

        this.items.sellIn = sellIn.decreaseSellIn(this.items.sellIn)

        return this.items;
    }
}

export default Standard