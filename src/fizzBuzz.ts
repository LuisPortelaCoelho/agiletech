const fizzBuzz = (parameter) => {
    if (parameter % 15 == 0) {
        return 'fizzbuzz'
    } 
    
    if (parameter % 5 == 0) {
        return 'buzz'
    } 
    
    if (parameter % 3 == 0) {
        return 'fizz'
    }

    return parameter.toString()
}

export default fizzBuzz;