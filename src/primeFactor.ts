class PrimeFactor {
    public allPrimeFactors(number) {

        if (number == 1) {
            return 'It is not prime number'
        }

        return this.calculatePrimeFactors(number)
    }

    private calculatePrimeFactors(number) {
        let prime = 2
        let listOfPrime = []
        let newNumber = number
        
        while (!(newNumber == prime)) {
            if (newNumber % prime == 0) {
                newNumber = newNumber / prime
                listOfPrime.push(prime)
            } 
            
            if (newNumber % prime != 0) {
                prime = prime + 1
            }
        }

        if (newNumber == prime) {
            listOfPrime.push(prime) 
        }

        return listOfPrime
    }

}

export default PrimeFactor