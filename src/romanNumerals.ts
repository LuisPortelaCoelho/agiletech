class RomanNumerals {
    //constructor(arabicNumerals) {
    //    this.arabicNumerals = arabicNumerals
    //}

    public convert(arabicNumerals) {
        const numberToString = arabicNumerals.toString()
        const numberToStringLength = numberToString.length
        
        if (numberToStringLength == 4) {
            return this.convertThousands(numberToString[0]) + this.convertHundreds(numberToString[1]) + this.convertTens(numberToString[2]) + this.convertUnits(numberToString[3])
        }
    
        if (numberToStringLength == 3) {
            return this.convertHundreds(numberToString[0]) + this.convertTens(numberToString[1]) + this.convertUnits(numberToString[2])
        }
    
        if (numberToStringLength == 2) {
            return this.convertTens(numberToString[0]) + this.convertUnits(numberToString[1])
        }
    
        if (numberToStringLength == 1) {
            return this.convertUnits(numberToString[0])
        }
    }

    private convertUnits(units: number) {
        if (units == 1) {
            return 'I'
        }

        if (units == 2) {
            return 'II'
        }
      
        if (units == 3) {
            return 'III'
        }

        if (units == 4) {
            return 'IV'
        }

        if (units == 5) {
            return 'V'
        }

        if (units == 6) {
            return 'VI'
        }

        if (units == 7) {
            return 'VII'
        }

        if (units == 8) {
            return 'VIII'
        }

        if (units == 9) {
            return 'IX'
        }

        return ''
    }

    private convertTens(tens: number) {
        if (tens == 1) {
            return 'X'
        }

        if (tens == 2) {
            return 'XX'
        }

        if (tens == 3) {
            return 'XXX'
        }

        if (tens == 4) {
            return 'XL'
        }

        if (tens == 5) {
            return 'L'
        }

        if (tens == 6) {
            return 'LX'
        }

        if (tens == 7) {
            return 'LXX'
        }

        if (tens == 8) {
            return 'LXXX'
        }

        if (tens == 9) {
            return 'XC'
        }

        return ''
    }

    private convertHundreds(hundreds: number) {
        if (hundreds == 1) {
            return 'C'
        }

        if (hundreds == 2) {
            return 'CC'
        }

        if (hundreds == 3) {
            return 'CCC'
        }

        if (hundreds == 4) {
            return 'CD'
        }

        if (hundreds == 5) {
            return 'D'
        }

        if (hundreds == 6) {
            return 'DC'
        }

        if (hundreds == 7) {
            return 'DCC'
        }

        if (hundreds == 8) {
            return 'DCCC'
        }

        if (hundreds == 9) {
            return 'CM'
        }
        
        return ''
    }

    private convertThousands(thousands: number) {
        if (thousands == 1) {
            return 'M'
        }

        if (thousands == 2) {
            return 'MM'
        }

        if (thousands == 3) {
            return 'MMM'
        }

        return ''
    }

} 

export default RomanNumerals