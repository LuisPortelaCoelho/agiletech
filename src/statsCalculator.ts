class StatsCalculator {
    array: []

    constructor(array) {
        this.array = array;
    }

    minimumValue() {
        return Math.min(...this.array)
    }

    maximumValue() {
        return Math.max(...this.array)
    }

    numberElementsInSequence() {
        return this.array.length
    }

    sumOfValues() {
        var total = 0
        for (var index in this.array) {
            total += this.array[index]
        }

        return total
    }
    
    averageValue() {
        return this.sumOfValues()/this.numberElementsInSequence()
    }

}

export default StatsCalculator