import Grid from "./grid"

class MarsRover {
    private instructions: string
    private xPosition: number
    private yPosition: number
    private direction: string
    private grid: Grid
    readonly changePosition = 'M'
    readonly availableDirections = ['N', 'E', 'S', 'W']
    readonly spins = ['R', 'L']

    constructor(initialPosition: string, instructions: string, grid: Grid) {
        this.instructions = instructions
        this.xPosition = parseInt(initialPosition[0])
        this.yPosition = parseInt(initialPosition[1])
        this.direction = initialPosition[2]
        this.grid = grid
    }

    private isToMoveNorth() {
        return this.direction == this.availableDirections[0]
    }

    private isToMoveEast() {
        return this.direction == this.availableDirections[1]
    }

    private isToMoveSouth() {
        return this.direction == this.availableDirections[2]
    }

    private isToMoveWest() {
        return this.direction == this.availableDirections[3]
    }

    public move() {
        for (let index = 0; index < this.instructions.length; index++) {
            if (this.instructions[index] != this.changePosition) {
                this.direction = this.rotate(this.instructions[index])
                continue
            }

            if (this.isToMoveNorth()) {
                this.yPosition = this.yPosition + 1
            }

            if (this.isToMoveEast()) {
                this.xPosition = this.xPosition + 1
            }

            if (this.isToMoveSouth()) {
                this.yPosition = this.yPosition - 1
            }

            if (this.isToMoveWest()) {
                this.xPosition = this.xPosition - 1
            }

            if (!this.grid.isInGrid(this.xPosition, this.yPosition)) {
                return 'It is out side of grid'
            }
        }
        return this.xPosition.toString() + this.yPosition.toString() + this.direction
    }

    private rotate(spin: string) {
        if (this.direction == this.availableDirections[3] && spin == this.spins[0]) {
            return this.availableDirections[0]
        }

        if (this.direction == this.availableDirections[0] && spin == this.spins[1]) {
            return this.availableDirections[3]
        }

        if (spin == this.spins[0]) {
            return this.availableDirections[this.currentPosition() + 1]
        }

        if (spin == this.spins[1]) {
            return this.availableDirections[this.currentPosition() - 1]
        }
    }

    private currentPosition() {
        return this.availableDirections.indexOf(this.direction)
    }
}

export default MarsRover