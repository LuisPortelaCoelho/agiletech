class Grid {
    private maximumWidth: number
    private maximumHeight: number
    readonly minimumWidth = 0
    readonly minimumHeight = 0

    constructor(maximumWidth: number, maximumHeight: number) {
        this.maximumWidth = maximumWidth
        this.maximumHeight = maximumHeight
    }

    private isInHeight(yPosition: number) {
        if (this.maximumHeight >= yPosition && yPosition >= this.minimumHeight) {
            return true
        }
        return false
    }

    private isInWidth(xPosition: number) {
        if (this.maximumWidth >= xPosition && xPosition >= this.minimumWidth) {
            return true
        }
        return false
    }

    public isInGrid(xPosition: number, yPosition: number) {
        if (this.isInWidth(xPosition) && this.isInHeight(yPosition)) {
            return true
        }
        return false
    }
}

export default Grid