import { TennisGame } from './TennisGame';


export class TennisGame3 implements TennisGame {
  private player1Score: number = 0;
  private player2Score: number = 0;
  private player1Name: string;
  private player2Name: string;

  constructor(player1Name: string, player2Name: string) {
    this.player1Name = player1Name;
    this.player2Name = player2Name;
  }

  private isSameScore(): boolean {
    return this.player1Score === this.player2Score
  }

  private differenceScoreBetweenPlayer1AndPlayer2(): number {
    return this.player1Score - this.player2Score
  }

  private sumScoreBetweenPlayer1AndPlayer2(): number {
    return this.player1Score + this.player2Score
  }

  getScore(): string {
    let score: string;

    if (this.player1Score <= 3 && this.player2Score <= 3 && this.sumScoreBetweenPlayer1AndPlayer2() !== 6) {
      const formatScoreWhenPlayersAreTied: string[] = ['Love', 'Fifteen', 'Thirty', 'Forty'];
      score = formatScoreWhenPlayersAreTied[this.player1Score];

      if (this.isSameScore()){
        return score + '-All'
      }

      return score + '-' + formatScoreWhenPlayersAreTied[this.player2Score];

    }

    if (this.isSameScore()) {
      return 'Deuce';
    }

    if (this.differenceScoreBetweenPlayer1AndPlayer2() > 0) {
      score = this.player1Name
    } 

    if (this.differenceScoreBetweenPlayer1AndPlayer2() < 0) {
      score = this.player2Name
    }

    if ((this.differenceScoreBetweenPlayer1AndPlayer2()) === 1 || this.differenceScoreBetweenPlayer1AndPlayer2() === -1) {
      return 'Advantage ' + score
    }

    return 'Win for ' + score;
  }

  private isPlayer1WonPoint(playerName: string): boolean {
    return playerName === 'player1'
  }

  private isPlayer2WonPoint(playerName: string): boolean {
    return playerName === 'player2'
  }

  public wonPoint(playerName: string): void {
    if (this.isPlayer1WonPoint(playerName)) {
      this.player1Score += 1;
    }
    if (this.isPlayer2WonPoint(playerName)) {
      this.player2Score += 1;
    }
  }
}