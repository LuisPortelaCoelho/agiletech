import { TennisGame } from './TennisGame';


export class TennisGame2 implements TennisGame {
  private player1point: number = 0;
  private player2point: number = 0;

  private player1result: string = '';
  private player2result: string = '';

  private player1Name: string;
  private player2Name: string;

  constructor(player1Name: string, player2Name: string) {
    this.player1Name = player1Name;
    this.player2Name = player2Name;
  }

  getScore(): string {
    let score: string = '';
    if (this.player1point === this.player2point && this.player1point < 4) {
      if (this.player1point === 0)
        return 'Love-All';
      if (this.player1point === 1)
        return 'Fifteen-All';
      if (this.player1point === 2)
        return 'Thirty-All';
    }

    if (this.player1point === this.player2point && this.player1point >= 3) {
      return 'Deuce';
    }

    if (this.player1point > 0 && this.player2point === 0) {
      if (this.player1point === 1)
        this.player1result = 'Fifteen';
      if (this.player1point === 2)
        this.player1result = 'Thirty';
      if (this.player1point === 3)
        this.player1result = 'Forty';

      this.player2result = 'Love';
      score = this.player1result + '-' + this.player2result;
    }

    if (this.player2point > 0 && this.player1point === 0) {
      if (this.player2point === 1)
        this.player2result = 'Fifteen';
      if (this.player2point === 2)
        this.player2result = 'Thirty';
      if (this.player2point === 3)
        this.player2result = 'Forty';

      this.player1result = 'Love';
      score = this.player1result + '-' + this.player2result;
    }

    if (this.player1point > this.player2point && this.player1point < 4) {
      if (this.player1point === 2)
        this.player1result = 'Thirty';
      if (this.player1point === 3)
        this.player1result = 'Forty';
      if (this.player2point === 1)
        this.player2result = 'Fifteen';
      if (this.player2point === 2)
        this.player2result = 'Thirty';
      score = this.player1result + '-' + this.player2result;
    }

    if (this.player2point > this.player1point && this.player2point < 4) {
      if (this.player2point === 2)
        this.player2result = 'Thirty';
      if (this.player2point === 3)
        this.player2result = 'Forty';
      if (this.player1point === 1)
        this.player1result = 'Fifteen';
      if (this.player1point === 2)
        this.player1result = 'Thirty';
      score = this.player1result + '-' + this.player2result;
    }

    if (this.player1point > this.player2point && this.player2point >= 3) {
      score = 'Advantage player1';
    }

    if (this.player2point > this.player1point && this.player1point >= 3) {
      score = 'Advantage player2';
    }

    if (this.player1point >= 4 && this.player2point >= 0 && this.differencePointsBetweenPlayer1AndPlayer2() >= 2) {
      score = 'Win for player1';
    }

    if (this.player2point >= 4 && this.player1point >= 0 && this.differencePointsBetweenPlayer1AndPlayer2() < -1) {
      score = 'Win for player2';
    }

    return score;
  }

  private differencePointsBetweenPlayer1AndPlayer2() {
    return this.player1point - this.player2point
  }

  private updateScore(player: string): void {
    if (player === 'player1') {
      this.player1point++;
    } else {
      this.player2point++;
    }
  }

  public wonPoint(player: string): void {
    this.updateScore(player)
  }
}
