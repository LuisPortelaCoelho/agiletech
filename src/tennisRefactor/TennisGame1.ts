import { TennisGame } from './TennisGame';


export class TennisGame1 implements TennisGame {
  private player1Score: number = 0;
  private player2Score: number = 0;
  private player1Name: string;
  private player2Name: string;

  constructor(player1Name: string, player2Name: string) {
    this.player1Name = player1Name;
    this.player2Name = player2Name;
  }

  private isPlayer1WonPoint(playerName: string): boolean {
    return playerName === 'player1'
  }

  public wonPoint(playerName: string): void {
    if (this.isPlayer1WonPoint(playerName))
      this.player1Score += 1;
    else
      this.player2Score += 1;
  }

  private equalPlayScore(): boolean {
    return this.player1Score === this.player2Score
  }
  
  private formatScoreWhenPlayersAreTied(tempScore: number) {
    switch (tempScore) {
      case 0:
        return 'Love';
      case 1:
        return 'Fifteen';
      case 2:
        return 'Thirty';
      case 3:
        return 'Forty';
    }
  }

  private scoreHigherThan4(): boolean {
    return this.player1Score >= 4 || this.player2Score >= 4
  }

  public getScore(): string {
    let score: string = '';
    let tempScore: number = 0;

    if (this.equalPlayScore()) {
      if (this.player1Score == 0) {
        return 'Love-All'
      }

      if (this.player1Score == 1) {
        return 'Fifteen-All'
      }

      if (this.player1Score == 2) {
        return 'Thirty-All'
      }
      return 'Deuce'
    }

    if (this.scoreHigherThan4()) {
      const minusResult: number = this.player1Score - this.player2Score;
      if (minusResult === 1) {
        return 'Advantage player1'
      }

      if (minusResult === -1) {
        return 'Advantage player2'
      }

      if (minusResult >= 2) {
        return 'Win for player1'
      }
      return 'Win for player2';
    }

    for (let index = 1; index < 3; index++) {
      if (index === 1) {
        tempScore = this.player1Score;
      }
      else {
        score += '-';
        tempScore = this.player2Score;
      }

      score += this.formatScoreWhenPlayersAreTied(tempScore)
    }

    return score;
  }
}